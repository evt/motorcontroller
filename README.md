# EVT Motor Controller

This is a 3-phase motor controller designed for the RIT Electric Vehicle Team by Senior Design Team P16261.

## Components
1. TI Launchxl F28069M LaunchPad
2. TI Boostxl DRV8301 BoosterPack

## Setup
### Launchpad with BoosterPack
On the Launchpad, connect jumpers JP1, JP2, and JP3. All others should be off. In order to debug, the third switch on S1 must be in the up position, the other two can be down.

Place the BoosterPack onto headers J1, J2, J3, and J4, with the power lines of the BoosterPack facing the same direction as the USB port on the Launchpad. Connect a 17V supply and ground to the BoosterPack. Connect the three phase wires to the motor.

## Pinout
How to connect the LaunchPad.

### ADC
Pin | Input | Header
----|-------|-------
A0 | EXT IA-FA | 27
B0 | EXT IB-FA | 28
A1 | EXT IC-FA | 29
B1 | ADC Vhb1 | 24
A2 | ADC Vhb2 | 25
B2 | ADC Vhb3 | 26
A7 | VDC Bus | 23
