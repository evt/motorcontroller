################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../include/modules/math/src/float/CLASinCosTable.asm \
../include/modules/math/src/float/CLAcos.asm \
../include/modules/math/src/float/CLAsin.asm \
../include/modules/math/src/float/CLAsqrt.asm \
../include/modules/math/src/float/FPUmathTables.asm \
../include/modules/math/src/float/sincos.asm \
../include/modules/math/src/float/sqrt.asm 

OBJS += \
./include/modules/math/src/float/CLASinCosTable.obj \
./include/modules/math/src/float/CLAcos.obj \
./include/modules/math/src/float/CLAsin.obj \
./include/modules/math/src/float/CLAsqrt.obj \
./include/modules/math/src/float/FPUmathTables.obj \
./include/modules/math/src/float/sincos.obj \
./include/modules/math/src/float/sqrt.obj 

ASM_DEPS += \
./include/modules/math/src/float/CLASinCosTable.pp \
./include/modules/math/src/float/CLAcos.pp \
./include/modules/math/src/float/CLAsin.pp \
./include/modules/math/src/float/CLAsqrt.pp \
./include/modules/math/src/float/FPUmathTables.pp \
./include/modules/math/src/float/sincos.pp \
./include/modules/math/src/float/sqrt.pp 

OBJS__QUOTED += \
"include\modules\math\src\float\CLASinCosTable.obj" \
"include\modules\math\src\float\CLAcos.obj" \
"include\modules\math\src\float\CLAsin.obj" \
"include\modules\math\src\float\CLAsqrt.obj" \
"include\modules\math\src\float\FPUmathTables.obj" \
"include\modules\math\src\float\sincos.obj" \
"include\modules\math\src\float\sqrt.obj" 

ASM_DEPS__QUOTED += \
"include\modules\math\src\float\CLASinCosTable.pp" \
"include\modules\math\src\float\CLAcos.pp" \
"include\modules\math\src\float\CLAsin.pp" \
"include\modules\math\src\float\CLAsqrt.pp" \
"include\modules\math\src\float\FPUmathTables.pp" \
"include\modules\math\src\float\sincos.pp" \
"include\modules\math\src\float\sqrt.pp" 

ASM_SRCS__QUOTED += \
"../include/modules/math/src/float/CLASinCosTable.asm" \
"../include/modules/math/src/float/CLAcos.asm" \
"../include/modules/math/src/float/CLAsin.asm" \
"../include/modules/math/src/float/CLAsqrt.asm" \
"../include/modules/math/src/float/FPUmathTables.asm" \
"../include/modules/math/src/float/sincos.asm" \
"../include/modules/math/src/float/sqrt.asm" 


