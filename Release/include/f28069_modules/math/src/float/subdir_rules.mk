################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
include/f28069_modules/math/src/float/CLASinCosTable.obj: ../include/f28069_modules/math/src/float/CLASinCosTable.asm $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-c2000_6.4.9/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/ti-cgt-c2000_6.4.9" --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/f28069_modules" --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/f28069_drivers" -g --define=FAST_ROM_V1p6 --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="include/f28069_modules/math/src/float/CLASinCosTable.pp" --obj_directory="include/f28069_modules/math/src/float" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

include/f28069_modules/math/src/float/CLAcos.obj: ../include/f28069_modules/math/src/float/CLAcos.asm $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-c2000_6.4.9/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/ti-cgt-c2000_6.4.9" --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/f28069_modules" --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/f28069_drivers" -g --define=FAST_ROM_V1p6 --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="include/f28069_modules/math/src/float/CLAcos.pp" --obj_directory="include/f28069_modules/math/src/float" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

include/f28069_modules/math/src/float/CLAsin.obj: ../include/f28069_modules/math/src/float/CLAsin.asm $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-c2000_6.4.9/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/ti-cgt-c2000_6.4.9" --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/f28069_modules" --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/f28069_drivers" -g --define=FAST_ROM_V1p6 --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="include/f28069_modules/math/src/float/CLAsin.pp" --obj_directory="include/f28069_modules/math/src/float" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

include/f28069_modules/math/src/float/CLAsqrt.obj: ../include/f28069_modules/math/src/float/CLAsqrt.asm $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-c2000_6.4.9/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/ti-cgt-c2000_6.4.9" --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/f28069_modules" --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/f28069_drivers" -g --define=FAST_ROM_V1p6 --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="include/f28069_modules/math/src/float/CLAsqrt.pp" --obj_directory="include/f28069_modules/math/src/float" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

include/f28069_modules/math/src/float/FPUmathTables.obj: ../include/f28069_modules/math/src/float/FPUmathTables.asm $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-c2000_6.4.9/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/ti-cgt-c2000_6.4.9" --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/f28069_modules" --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/f28069_drivers" -g --define=FAST_ROM_V1p6 --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="include/f28069_modules/math/src/float/FPUmathTables.pp" --obj_directory="include/f28069_modules/math/src/float" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

include/f28069_modules/math/src/float/sincos.obj: ../include/f28069_modules/math/src/float/sincos.asm $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-c2000_6.4.9/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/ti-cgt-c2000_6.4.9" --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/f28069_modules" --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/f28069_drivers" -g --define=FAST_ROM_V1p6 --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="include/f28069_modules/math/src/float/sincos.pp" --obj_directory="include/f28069_modules/math/src/float" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

include/f28069_modules/math/src/float/sqrt.obj: ../include/f28069_modules/math/src/float/sqrt.asm $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-c2000_6.4.9/bin/cl2000" -v28 -ml -mt -O2 --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/ti-cgt-c2000_6.4.9" --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/f28069_modules" --include_path="C:/Users/Matthew/Documents/evt/copy/proj_lab05a/include/f28069_drivers" -g --define=FAST_ROM_V1p6 --diag_warning=225 --display_error_number --preproc_with_compile --preproc_dependency="include/f28069_modules/math/src/float/sqrt.pp" --obj_directory="include/f28069_modules/math/src/float" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


