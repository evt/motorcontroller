################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../include/f28069_modules/math/src/float/CLASinCosTable.asm \
../include/f28069_modules/math/src/float/CLAcos.asm \
../include/f28069_modules/math/src/float/CLAsin.asm \
../include/f28069_modules/math/src/float/CLAsqrt.asm \
../include/f28069_modules/math/src/float/FPUmathTables.asm \
../include/f28069_modules/math/src/float/sincos.asm \
../include/f28069_modules/math/src/float/sqrt.asm 

OBJS += \
./include/f28069_modules/math/src/float/CLASinCosTable.obj \
./include/f28069_modules/math/src/float/CLAcos.obj \
./include/f28069_modules/math/src/float/CLAsin.obj \
./include/f28069_modules/math/src/float/CLAsqrt.obj \
./include/f28069_modules/math/src/float/FPUmathTables.obj \
./include/f28069_modules/math/src/float/sincos.obj \
./include/f28069_modules/math/src/float/sqrt.obj 

ASM_DEPS += \
./include/f28069_modules/math/src/float/CLASinCosTable.pp \
./include/f28069_modules/math/src/float/CLAcos.pp \
./include/f28069_modules/math/src/float/CLAsin.pp \
./include/f28069_modules/math/src/float/CLAsqrt.pp \
./include/f28069_modules/math/src/float/FPUmathTables.pp \
./include/f28069_modules/math/src/float/sincos.pp \
./include/f28069_modules/math/src/float/sqrt.pp 

OBJS__QUOTED += \
"include\f28069_modules\math\src\float\CLASinCosTable.obj" \
"include\f28069_modules\math\src\float\CLAcos.obj" \
"include\f28069_modules\math\src\float\CLAsin.obj" \
"include\f28069_modules\math\src\float\CLAsqrt.obj" \
"include\f28069_modules\math\src\float\FPUmathTables.obj" \
"include\f28069_modules\math\src\float\sincos.obj" \
"include\f28069_modules\math\src\float\sqrt.obj" 

ASM_DEPS__QUOTED += \
"include\f28069_modules\math\src\float\CLASinCosTable.pp" \
"include\f28069_modules\math\src\float\CLAcos.pp" \
"include\f28069_modules\math\src\float\CLAsin.pp" \
"include\f28069_modules\math\src\float\CLAsqrt.pp" \
"include\f28069_modules\math\src\float\FPUmathTables.pp" \
"include\f28069_modules\math\src\float\sincos.pp" \
"include\f28069_modules\math\src\float\sqrt.pp" 

ASM_SRCS__QUOTED += \
"../include/f28069_modules/math/src/float/CLASinCosTable.asm" \
"../include/f28069_modules/math/src/float/CLAcos.asm" \
"../include/f28069_modules/math/src/float/CLAsin.asm" \
"../include/f28069_modules/math/src/float/CLAsqrt.asm" \
"../include/f28069_modules/math/src/float/FPUmathTables.asm" \
"../include/f28069_modules/math/src/float/sincos.asm" \
"../include/f28069_modules/math/src/float/sqrt.asm" 


