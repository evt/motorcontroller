################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CLA_SRCS += \
../include/f28069_modules/filter/src/float/filter_fo.cla \
../include/f28069_modules/filter/src/float/filter_so.cla 

C_SRCS += \
../include/f28069_modules/filter/src/float/filter_fo.c \
../include/f28069_modules/filter/src/float/filter_so.c 

OBJS += \
./include/f28069_modules/filter/src/float/filter_fo.obj \
./include/f28069_modules/filter/src/float/filter_so.obj 

CLA_DEPS += \
./include/f28069_modules/filter/src/float/filter_fo.pp \
./include/f28069_modules/filter/src/float/filter_so.pp 

C_DEPS += \
./include/f28069_modules/filter/src/float/filter_fo.pp \
./include/f28069_modules/filter/src/float/filter_so.pp 

CLA_DEPS__QUOTED += \
"include\f28069_modules\filter\src\float\filter_fo.pp" \
"include\f28069_modules\filter\src\float\filter_so.pp" 

C_DEPS__QUOTED += \
"include\f28069_modules\filter\src\float\filter_fo.pp" \
"include\f28069_modules\filter\src\float\filter_so.pp" 

OBJS__QUOTED += \
"include\f28069_modules\filter\src\float\filter_fo.obj" \
"include\f28069_modules\filter\src\float\filter_so.obj" 

C_SRCS__QUOTED += \
"../include/f28069_modules/filter/src/float/filter_fo.c" \
"../include/f28069_modules/filter/src/float/filter_so.c" 


